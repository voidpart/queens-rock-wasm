extern crate queens_rock;
extern crate wasm_bindgen;
#[macro_use]
extern crate serde_derive;

use queens_rock::Scanner as ImgScanner;
use wasm_bindgen::prelude::*;

#[derive(Serialize)]
struct CodeResult {
    pub error: Option<String>,
    pub text: Option<String>,
}

#[wasm_bindgen]
pub fn scan(img: &[u8], w: usize, h: usize) -> JsValue {
    let scanner = ImgScanner::new(img, w, h);
    let scan_results = scanner.scan();

    let mut results: Vec<CodeResult> = Vec::new();
    for i in 0..scan_results.len() {
        let code = scan_results
            .extract(i)
            .unwrap()
            .decode()
            .and_then(|i| i.try_string());

        match code {
            Ok(data) => {
                results.push(CodeResult {
                    error: None,
                    text: Some(data),
                });
            }
            Err(_) => {
                results.push(CodeResult {
                    error: Some(String::from("Something went wrong")),
                    text: None,
                });
            }
        };
    }

    JsValue::from_serde(&results).unwrap()
}

#[wasm_bindgen]
pub struct BufferScanner {
    buffer: Vec<u8>,
    width: usize,
    height: usize,
}

#[wasm_bindgen]
impl BufferScanner {
    #[wasm_bindgen(constructor)]
    pub fn new(width: usize, height: usize) -> BufferScanner {
        let mut buffer = Vec::new();
        buffer.resize(width * height, 0);

        BufferScanner {
            buffer: buffer,
            width: width,
            height: height,
        }
    }

    pub fn set(&mut self, x: usize, y: usize, value: u8) {
        self.buffer[y * self.width + x] = value;
    }

    pub fn scan(&self) -> JsValue {
        scan(self.buffer.as_ref(), self.width, self.height)
    }
}
