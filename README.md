# Queen's rock (WASM wrapper)

Lightweight QR-code extracting and decoding library.

This repository is simple wrapper around rust crate.

## Build

```
wasm-pack build
```
